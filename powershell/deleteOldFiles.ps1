# Delete all Files older than 30 day(s)

$Location="C:\Users\Administrator\Desktop\test"
$DaysBack="-30"
$Today=Get-Date -UFormat %d-%m-%Y
$LogFile="$Location\$Today.log"

# Log file
Write-Output "Delete Files Older Than 30 Days" | Out-File $LogFile
Write-Output "Date:  $(Get-Date)" | Out-File -Append $LogFile
Write-Output "List of Files For Removing:" | Out-File -Append $LogFile

# Write a files list to the log file
Get-ChildItem -Path $Location -Recurse | Where-Object {$_.LastWriteTime -lt (Get-Date).AddDays($DaysBack)} |Select-Object -Property Name,CreationTime,LastWriteTime| Out-File -Append $LogFile

# Deleting Files
Get-ChildItem -Path $Location -Recurse | Where-Object {$_.LastWriteTime -lt (Get-Date).AddDays($DaysBack)} | Remove-Item -Force
Write-Output "-------------------- Finished --------------------" | Out-File -Append $LogFile